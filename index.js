const intial = "GUVF NCCYVPNGVBA PERNGR OL NUZRQ FBHN";

class App extends React.Component {
    state = {
        text: intial
    };

    handleChange = (e) => {
        this.setState({
            text:e.target.value
        });
    };
    
    rot13=(str)=>{
        str=str.toUpperCase();
        let char=""; 
        let newstr="";
        let l=str.length;
        for(var i=0;i<l;i++){
          if((str.charCodeAt(i)>=65) && (str.charCodeAt(i)<=77)){
            char=String.fromCharCode(str.charCodeAt(i)+13);
            newstr+=char;
           }
            else if 
            ((str.charCodeAt(i)>=78) && (str.charCodeAt(i)<=90)){
            char=String.fromCharCode(str.charCodeAt(i)-13);
            newstr+=char;
            }
            else {newstr+=str[i];}
            }
          return newstr;
        }

    render() {

        const {text} = this.state;
    
        return (
            <div>
                <div className="intro">
                    <h1><u>Caesars Cipher</u></h1>
                    <p>One of the simplest and most widely known ciphers is a Caesar cipher, also known as a shift cipher. In a shift cipher the meanings of the letters are shifted by some set amount.</p>
                    <p>A common modern use is the ROT13 cipher, where the values of the letters are shifted by 13 places. Thus 'A' ↔ 'N', 'B' ↔ 'O' and so on.</p>
                    <p>
                        <i class="fas fa-keyboard"></i>
                        <i class="fas fa-long-arrow-alt-right"></i>
                        <i class="fas fa-shield-alt"></i>
                        <i class="fas fa-long-arrow-alt-right"></i>
                        <i class="far fa-keyboard"></i>
                    </p>
                </div>
                <div className=" work">
                    <div className="editor">
                        <h6>Enter your script:</h6>
                        <textarea
                            id="editor"
                            className=""
                            value={text}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="preview">
                        <h6>See your crypto:</h6>
                        <div id="preview" className="">
                            {this.rot13(text)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(<App/>, document.getElementById("app"));
